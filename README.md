#### insert in to `head`
```html
   <meta name="smartbanner:title" content="Атлант-м">
   <meta name="smartbanner:author" content="Атлант-м">
   <meta name="smartbanner:price" content="Цена:">
   <meta name="smartbanner:price-suffix-apple" content=" ЗАГРУЗИТЬ в App Storee">
   <meta name="smartbanner:price-suffix-google" content=" ЗАГРУЗИТЬ в Google Play">
   <meta name="smartbanner:icon-apple" content="smartbanner/icon_58.png">
   <meta name="smartbanner:icon-google" content="smartbanner/icon_58.png">
   <meta name="smartbanner:button" content="Смотреть">
   <meta name="smartbanner:button-url-apple" content="https://itunes.apple.com/ru/app/atlant-m/id515931794?mt=8">
   <meta name="smartbanner:button-url-google" content="https://play.google.com/store/apps/details?id=com.atlantm">
   <meta name="smartbanner:enabled-platforms" content="android,ios">
   <link rel="stylesheet" href="smartbanner/smartbanner.css">
   <script src="smartbanner/smartbanner.js"></script>
```

---

based on: [https://github.com/ain/smartbanner.js](https://github.com/ain/smartbanner.js)

re-worked in Ahswood Creative: [http://ashwood.by](http://ashwood.by)

Developer: [https://github.com/mepfunc](https://github.com/mepfunc)

Contacts: [https://t.me/x1110](https://t.me/x1110)

#### Copyright © 2018 Ain Tohvri, contributors. Licensed under GPL-3.0.
